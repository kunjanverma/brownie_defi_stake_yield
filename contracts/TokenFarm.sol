//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import '@openzeppelin/contracts/access/Ownable.sol';
import '@openzeppelin/contracts/token/ERC20/IERC20.sol';
import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

contract TokenFarm is Ownable
{
    address[] public allowedTokenList;
    mapping(address => mapping(address => uint256)) public stakingBalances;//token address->user address->amount
    address [] public stakersList;
    mapping (address => uint256) public uniqueTokensStaked;
    mapping (address => address) public tokenPriceFeedMapping;

    IERC20 public dappToken;

    constructor(address _dappTokenAddress)
    {
        dappToken = IERC20(_dappTokenAddress);
    }

    function setPriceFeedContract(address _tokenAddress, address _priceFeedAddress) public onlyOwner
    {
        tokenPriceFeedMapping[_tokenAddress] = _priceFeedAddress;
    }

    function tokenIsAllowed(address _tokenAddress) public view returns(bool)
    {
        for(uint256 idx=0; idx< allowedTokenList.length; idx++)
        {
            if (allowedTokenList[idx] == _tokenAddress)
                return true;
        }
        return false;
    }

    function addAllowedToken(address _tokenAddress) public onlyOwner
    {
        allowedTokenList.push(_tokenAddress);
    }

    function updateUniqueTokensStaked(address _owner, address _tokenAddress) internal
    {
        if (stakingBalances[_tokenAddress][_owner] <= 0) //only update uniqueTokensStaked if 
                                                         //this user has zero amount of this token staked
        {
           uniqueTokensStaked[_owner] += 1;
        }
    }

    function getTokenValue(address _tokenAddress) internal view returns(uint256,uint256)
    {
        address priceFeedAddress = tokenPriceFeedMapping[_tokenAddress];
        AggregatorV3Interface priceFeed = AggregatorV3Interface(priceFeedAddress);
        (,int256 price,,,) = priceFeed.latestRoundData();
        uint256 decimals = uint256(priceFeed.decimals());
        return(uint256(price),decimals);
    }

    function getSingleUserTokenValue(address _user, address _tokenAddress) public view returns(uint256)
    {
        if(uniqueTokensStaked[_user] <= 0)
            return 0;

        (uint256 price, uint256 decimals) =  getTokenValue(_tokenAddress);
        return (stakingBalances[_tokenAddress][_user] * price/10**decimals);
    }

    function getUserTotalvalue(address _user) public view returns(uint256)
    {
        uint256 totalValue = 0;
        require(uniqueTokensStaked[_user] > 0, "No tokens staked");
        for(uint256 idx=0; idx<allowedTokenList.length;idx++)
        {
            totalValue += getSingleUserTokenValue(_user, allowedTokenList[idx]);
        }
        return totalValue;
    }

    function stakeToken(uint256 _amount, address _tokenAddress) public
    {
        require(_amount>0,"Amount must be greater than zero!");
        require(tokenIsAllowed(_tokenAddress), "Cannot stake this token, its not allowed");
        IERC20(_tokenAddress).transferFrom(msg.sender, address(this), _amount);
        updateUniqueTokensStaked(msg.sender ,_tokenAddress);
        stakingBalances[_tokenAddress][msg.sender] += _amount;
        if (uniqueTokensStaked[msg.sender] == 1)
            stakersList.push(msg.sender);
    }

    function unstakeToken(address _tokenAddress) public
    {
        uint256 balance = stakingBalances[_tokenAddress][msg.sender];
        require(balance > 0, "Staking balance cannot be 0");
        IERC20(_tokenAddress).transfer(msg.sender, balance);
        stakingBalances[_tokenAddress][msg.sender] = 0;
        uniqueTokensStaked[msg.sender] -= 1;
        if(uniqueTokensStaked[msg.sender] <= 0)
        {
            for(uint256 idx=0; idx < stakersList.length; idx++)
            {
                if (stakersList[idx] == msg.sender)
                {
                    stakersList[idx] = stakersList[stakersList.length-1];//copy last element in this one
                    stakersList.pop();//now delete the last element
                }
            }
        }
    }

    function issueToken() public onlyOwner
    {
        for(uint256 idx; idx < stakersList.length; idx++)
        {
            address recipient = stakersList[idx];
            uint256 userTotalValue =  getUserTotalvalue(recipient);
            dappToken.transfer(recipient,userTotalValue);
        }
    }

}