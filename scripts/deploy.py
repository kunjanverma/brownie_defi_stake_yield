import scripts.helpers
from web3 import Web3
from brownie import DappToken, TokenFarm, config, network

KEPT_BALANCE = Web3.toWei(100, "ether")

def add_allowed_tokens(tokenFarm, tokenAddr_to_pricefeed_dict, account):
    for token in tokenAddr_to_pricefeed_dict:
        tx = tokenFarm.addAllowedToken(token.address, {"from":account})
        tx.wait(1)
        tx = tokenFarm.setPriceFeedContract(token.address, tokenAddr_to_pricefeed_dict[token], {"from":account})
        tx.wait(1)
    
def deploy_token_farm_and_dapp_token():
    deploy_account = scripts.helpers.get_account(0)
    dapp_token = DappToken.deploy({"from" : deploy_account},
                                   publish_source=config["networks"][network.show_active()].get("verify", False))
    token_farm = TokenFarm.deploy(dapp_token.address,
                                  {"from" : deploy_account},
                                   publish_source=config["networks"][network.show_active()].get("verify", False))
    tx = dapp_token.transfer(token_farm.address, dapp_token.totalSupply()-KEPT_BALANCE, {"from":deploy_account})
    tx.wait(1)

    #eth, dapp, faucet
    weth_token = scripts.helpers.get_contract("weth_token")
    faucet_token = scripts.helpers.get_contract("fau_token")
    tokenAddr_to_pricefeed_dict = {
        dapp_token : scripts.helpers.get_contract("dai_usd_price_feed"),
        faucet_token : scripts.helpers.get_contract("dai_usd_price_feed"),
        weth_token : scripts.helpers.get_contract("eth_usd_price_feed")
    }

    add_allowed_tokens(token_farm, tokenAddr_to_pricefeed_dict, deploy_account)
    return dapp_token, token_farm

def check_and_deploy():
    dapp_token = None
    token_farm = None
    if len(DappToken) <= 0 or len(TokenFarm) < 0:
        dapp_token, token_farm = deploy_token_farm_and_dapp_token()
    else:
        dapp_token = DappToken[-1]
        token_farm = TokenFarm[-1]
        
    return dapp_token, token_farm
    
def main():
    #deploy_token_farm_and_dapp_token()
    check_and_deploy()