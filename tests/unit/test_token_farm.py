from brownie import network, exceptions
import scripts.deploy
import scripts.helpers
import pytest
from web3 import Web3

def test_set_price_feed_contract():
    if network.show_active() not in scripts.helpers.LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("This test is only for local testing")
    deploy_account = scripts.helpers.get_account(0)
    user1_account = scripts.helpers.get_account(1)
    dapp_token, token_farm = scripts.deploy.deploy_token_farm_and_dapp_token()

    price_feed_addr = scripts.helpers.get_contract("eth_usd_price_feed").address
    token_farm.setPriceFeedContract(dapp_token.address, 
                                    price_feed_addr,
                                    {"from":deploy_account})
    assert(token_farm.tokenPriceFeedMapping(dapp_token.address)==price_feed_addr)

    with pytest.raises(exceptions.VirtualMachineError):
        token_farm.setPriceFeedContract(dapp_token.address, 
                                        price_feed_addr,
                                        {"from":user1_account})

def test_allowed_tokens():
    if network.show_active() not in scripts.helpers.LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("This test is only for local testing")
    deploy_account = scripts.helpers.get_account(0)
    dapp_token, token_farm = scripts.deploy.deploy_token_farm_and_dapp_token()
    assert(token_farm.tokenIsAllowed(scripts.helpers.get_contract("weth_token").address) == True)
    assert(token_farm.tokenIsAllowed(scripts.helpers.get_contract("fau_token").address) == True)
    assert(token_farm.tokenIsAllowed(dapp_token.address) == True)
    assert(token_farm.tokenIsAllowed("0xd0a1e359811322d97991e03f863a0c30c2cf029f") == False)#arbitary address

def test_stake_tokens(amount_staked):
    if network.show_active() not in scripts.helpers.LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("This test is only for local testing")
    deploy_account = scripts.helpers.get_account(0)
    user1_account = scripts.helpers.get_account(1)
    dapp_token, token_farm = scripts.deploy.deploy_token_farm_and_dapp_token()
    weth_token = scripts.helpers.get_contract("weth_token")
    weth_token.transfer(user1_account, Web3.toWei(100, "ether"), {"from":deploy_account})
    weth_token.approve(token_farm.address, amount_staked, {"from":user1_account})
    token_farm.stakeToken(amount_staked, weth_token.address, {"from":user1_account})
    assert(token_farm.stakingBalances(weth_token.address, user1_account) == amount_staked)
    assert(token_farm.uniqueTokensStaked(user1_account) == 1)
    assert(token_farm.stakersList(0) == user1_account)
    return dapp_token, token_farm 

def test_issue_tokens(amount_staked):
    if network.show_active() not in scripts.helpers.LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("This test is only for local testing")
    deploy_account = scripts.helpers.get_account(0)
    user1_account = scripts.helpers.get_account(1)
    dapp_token, token_farm = test_stake_tokens(amount_staked)
    token_farm_dapp_balance = dapp_token.balanceOf(token_farm.address)
    token_farm.issueToken({"from":deploy_account})
    assert(dapp_token.balanceOf(deploy_account) == scripts.deploy.KEPT_BALANCE)
    assert(dapp_token.balanceOf(token_farm.address) == token_farm_dapp_balance -  amount_staked*scripts.helpers.INITIAL_PRICE_FEED_VALUE/10**scripts.helpers.DECIMALS)
    assert(dapp_token.balanceOf(user1_account) == amount_staked*scripts.helpers.INITIAL_PRICE_FEED_VALUE/10**scripts.helpers.DECIMALS)

def test_unstake_tokens(amount_staked):
    if network.show_active() not in scripts.helpers.LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip("This test is only for local testing")
    deploy_account = scripts.helpers.get_account(0)
    user1_account = scripts.helpers.get_account(1)
    weth_token = scripts.helpers.get_contract("weth_token")
    dapp_token, token_farm = test_stake_tokens(amount_staked)
    tx = token_farm.unstakeToken(weth_token,{"from":user1_account})
    tx.wait(1)
    assert(token_farm.stakingBalances(weth_token.address, user1_account) == 0)
    assert(token_farm.uniqueTokensStaked(user1_account) == 0)